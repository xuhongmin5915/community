# SIG Introduction

AI Security SIG focuses on security issues in AI model development projects from data processing, model training, and deployment inference. The details are as follows:

1. **AI Model's robustness and reliability**: Inference robustness to adversarial samples or natural perturbation samples. Data concept drift detection and model fault injection.
2. **Privacy protection**: Differential privacy training and federated learning.
3. **Model deployment security**: Model encryption and model obfuscation.

## SIG code repositories

1. [MindAarmour](https://gitee.com/mindspore/mindarmour).
2. [Federated learning's server](https://gitee.com/mindspore/mindspore/tree/master/mindspore/ccsrc/fl), [Federated learning's client](https://gitee.com/mindspore/mindspore/tree/master/mindspore/lite/java/java/fl_client/src/main/java/com/mindspore/flclient).

## SIG maintainers

* Wang Ze (Huawei)
* Xiulang (Huawei)

## SIG members description

The membership of the AI Security SIG includes Members, Reviewers, Committers, and Maintainers, and is recorded in the [members list](./sig_members.yaml). The description and application conditions of each role are as follows:

### Members

* Full member of the SIG, who can participate in the daily discussions and activities of the SIG.
* Application conditions: Attended at least two SIG conferences/events.

### Reviewers

* Code reviewer of the SIG code repository, responsible for reviewing the PR (pull requests) of the code repository.
* Application conditions: As a Member first, and have reviewed at least two PRs of the [MindSpore main warehouse](https://gitee.com/mindspore/mindspore/pulls) or [MindArmour warehouse](https://gitee.com/mindspore/mindarmour/pulls), and the total number of comments in the PR is greater than 10.

### Committers

* Committer of the SIG code repository, who has the permission to merge code repository PRs.
* Application conditions: As a reviewer, and have merged at least five PRs to **SIG code repository**.

### Maintainers

* Responsible for SIG's technical research direction and activity organizations.
* Application conditions: As a Committer and have passed the Maintainers qualification review (the organization will be organized after someone applies for it).

### Application Method

1. Fork [Community repository](https://gitee.com/mindspore/community).
2. Submit a PR. Add your Gitee Home link, name (real name is not required), and email address for registering Gitee to the [sig_members](./sig_members.yaml). Add proof of application, such as reviewed PRs and merged PRs, to the PR description. After the PR is submitted, Maintainers will review the PR.

## Recent Events Preview

## Previous Sessions

* [meeting video](https://www.bilibili.com/video/BV14g411V7nZ?spm_id_from=333.999.0.0)
* [2022-3-15 meeting forecast](https://mp.weixin.qq.com/s/NCw-kdQiTGXhH1BNrPiFkQ)
* [Thursday June 04, 2020](./meetings/001-20200604.md)
* [Friday July 03, 2020](./meetings/002-20200703.md)
* [Saturday August 08, 2020](./meetings/003-20200808.md)
* [Friday September 04, 2020](./meetings/004-20200904.md)
