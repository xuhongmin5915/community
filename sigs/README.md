# SIGs - Special Interest Groups

As described in the MindSpore [governance](../governance.md), Special
Interest Groups (SIGs) are persistent groups responsible for specific parts of
the project. SIGs have open and transparent proceedings to develop goals and
implement code contributions. SIGs are also responsible for ongoing maintenance
of the code in their areas.

## Current SIGs

| SIG name                                   | Responsibilities | SIG Leads |
|:-------------------------------------------| :--------------- | :-------- |
| [FrontEnd](frontend/README.md)             | This SIG is responsible for the development of MindSpore front-end expression. | [@wangnan](https://gitee.com/wangnan39) |
| [Compiler](compiler/README.md)             | This SIG is responsible for the development of MindSpore high level graph compilation. | [@zh_qh](https://gitee.com/zh_qh) |
| [Executor](executor/README.md)             | This SIG is responsible for the development of MindSpore back-end support for pipeline. | [@kisnwang](https://gitee.com/kisnwang) |
| [ModelZoo](modelzoo/README.md)             | This SIG is responsible for the development of MindSpore modelzoo and additional ops. | [@chenhaozhe](https://gitee.com/c_34) |
| [Data](data/README.md)                     | This SIG is responsible for the development of MindSpore data processing and data format transformation. | [@liucunwei](https://gitee.com/liucunwei) |
| [Visualization](visualization/README.md)   | This SIG is responsible for the development of Visualized debugging and optimization. | [@liangyongxiong](https://gitee.com/liangyongxiong1024) |
| [Usability](usability/README.md)           | This SIG is responsible for the improve the usability of MindSpore for developers. | [@zhangtong](https://gitee.com/tong-zhang) |
| [AI Security](security/README.md)          | This SIG is responsible for the development of MindSpore security related tools. | [@randywangze](https://gitee.com/randywangze) |
| [MSLITE](mslite/README.md)                 | This SIG is responsible for the development of MindSpore lite. | [@zhaizhiqiang](https://gitee.com/zhaizhiqiang) |
| [Parallel](parallel/README.md)             | This SIG is responsible for the development of MindSpore's functionality of automatically finding the efficient parallel strategy for DNN training and inference. | [@baiyouhui](https://gitee.com/bert0108) |
| [DevelopereXperience](dx/README.md)        | This SIG is responsible for improving the experience of those who upstream contribute or develop applications for MindSpore community. | [@jiancao81](https://gitee.com/jiancao81)(cao-jian@cs.sjtu.edu.cn)  |
| [DataCompliance](datacompliance/README.md) | This SIG aims to reduce the risk of license compliance and help developers to use and share datasets legally. | [@gopikrishnanrajbahadur](https://gitee.com/gopikrishnanrajbahadur) [@clement_li](https://gitee.com/clement_li) |

## Resources

You can view the screen recordings of the SIG regular meeting on the official bilibili account of MindSpore.

 [links](https://space.bilibili.com/526894060/channel/seriesdetail?sid=675044)

## Joining a SIG

If you are interested in participating, here are 2 ways to join the above SIGs:

1, Send messages to the SIG leads of the above form to introduce your basic situation and application intentions;

2, Pay attention to MindSpore's WeChat public account "MindSpore", we will release the SIG regular meeting information on the public account, and the organizer will publish the WeChat group QR code in the meeting.

## Proposing a new SIG

New SIGs are created when there is sufficient interest in a topic area
and someone volunteers to be the lead for the group and submits a proposal to
the steering committee. The chair facilitates the discussion and helps
synthesize proposals and decisions.

[Propose now!](https://gitee.com/mindspore/community/blob/master/sigs/dx/docs/How%20to%20build%20a%20SIG%20or%20WG_cn.md)
